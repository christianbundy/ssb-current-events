const wiki = require('wikijs').default

const download = query => new Promise((resolve, reject) => {
  wiki()
    .page(query)
    .then(page => page.html())
    .then(html => resolve(html))
    .catch(reject)
})

module.exports = download
